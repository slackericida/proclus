function [ M ] = Initialization( dataset, A, Bk, distFun )
%INITIALIZATION fase 1 dell'algoritmo: viene selezionato un set di potenziali medoid
%
% ----- INPUT -----
% dataset:
% A: regola dimensione del random sample
% B: regola quantita' di medoid da selezionare
% k: numero di cluster desiderati
% distfun: funzione di dissimilarita' usata per confrontare i dati
%
% ----- OUTPUT -----
% M: set dei medoid candidati


% random sample iniziale del dataset (serve per ridurre quantita' dei dati e outliers)
indS = randperm(size(dataset,1));
S = dataset(indS(1:A),:);

% viene selezionato un set M di medoids
M = Greedy(S, Bk, distFun);

end

function [M] = Greedy(S, Bk, distFun)
% GREEDY ricerca greedy di B medoid, che siano fra loro piu' distanziati possibile

Sindex = 1:length(S);

% selezione del primo rappresentante
firstind = randsample(Sindex,1);
Sindex = setdiff(Sindex,firstind); % indici dei pattern NON selezionati come medoids
dS = zeros(length(Sindex),1); % inizializzo container delle distanze di ciascun pattern dal medoid piu' vicino

% calcolo tutte le distanze dal primo rappresentante
for i=1:length(Sindex)
    dS(i) = distFun(S(firstind,:), S(Sindex(i),:));
end

% selezione degli altri rappresentanti
for i=2:Bk
    [~,maxind] = max(dS);
    
    % aggiorno le distanze dopo la selezione di un nuovo rappresentante
    for j=1:length(Sindex)
        dj = distFun( S(Sindex(maxind),:), S(Sindex(j),:) );
        if(dj < dS(j))
            dS(j) = dj;
        end
    end
    
    % rimuovo il candidato selezionato da quelli ancora disponibili
    dS(maxind) = [];
    Sindex = setdiff(Sindex,Sindex(maxind));
end

M = S(setdiff(1:length(S),Sindex),:);

end

