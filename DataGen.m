function [z] = DataGen(N, D, M, S)
% Genera N vettori in uno spazio a D dimensioni estratti da M gaussiane. S regola la larghezza della gaussiana

% z=[];

% for i=1:M
%     %genera la matrice di covarianza
%     A = rand(D,D);
%     A = A+A';
%     A = A + D*eye(D);
%     R = chol(A);
%     
%     mu = randn(1,D)*randn*S;
%     
%     z = [z;repmat(mu,N,1) + randn(N,D)*R];
%     
% end

c1 = [mvnrnd(rand, 0.001, N), mvnrnd(rand, 0.001, N), mvnrnd(rand, 0.1, N)];
c2 = [mvnrnd(rand, 0.1, N), mvnrnd(rand, 0.001, N), mvnrnd(rand, 0.001, N)];
c3 = [mvnrnd(rand, 0.001, N), mvnrnd(rand, 0.1, N), mvnrnd(rand, 0.001, N)];
z = [c1;c2;c3];

scatter3(z(:,1), z(:,2), z(:,3),'b')
end

