function [ dist ] = ManhattanDist( p1, p2, varargin )
%MANHATTANDIST distanza di manhattan pesata e riscalata per le dimensioni

dims = ones(size(p1));

if(nargin == 3)
    dims = varargin{1};
end

dist = abs(p1(:)-p2(:))' * dims(:) / nnz(dims);

end

