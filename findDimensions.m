function [D] = findDimensions(M, l, L, distFun, dataset)
% FINDDIMENSIONS trova l'insieme di dimensioni rilevanti per ciascun cluster i, definito dall'i-esimo centroide in M
% e da gli elementi nella lista i-esima in L, che rappresentano il suo vicinato.
%
% ----- INPUT ------
% M: set dei medoids
% l: numero massimo di dimensioni da selezionare
% L: vicinato di ciascun medoid
% distFun: funzione di dissimilarita' usata per confrontare i patterns
%
% ----- OUTPUT -----
% D: set dimensioni rilevanti per ciascun medoid

% inizializzazione strutture dati e variabili
k = size(M,1); % numero di medoids
D = cell(k,1); % set delle dimensioni rilevanti per ciascun medoid
Z = []; % valore di qualita' per ciascuna dimensione
nDims = size(M,2); % numero di dimensioni del dataset

for i=1:k
    mi = M(i,:);
    index_i = find(L(i,:));
    Xi = zeros(nDims,1);

    % calcolo la dispersione Xij per ogni medoid i lungo la dimensione j sul suo vicinato Li
    for j=1:nDims
        Xij = 0;
        dj = zeros(1,nDims);
        dj(j) = 1;            
        for p=1:length(index_i)
            Xij = Xij + distFun(mi, dataset(index_i(p),:), dj);
        end
        Xij = Xij/(length(index_i)-1); % sottraggo 1 perch� in Li c'� anche il medoid stesso
        Xi(j) = Xij;
    end

    Yi = sum(Xi)/nDims;
    Sigmai = sqrt(sum((Xi-Yi).^2)/(nDims-1));
    Zval_i = (Xi-Yi)/Sigmai;
    % costruisco il vettore Z con i seguenti campi: Z = [ clustID, dimID, Zval ]
    Zi = [ones(nDims,1)*i, (1:nDims)', Zval_i];

    % inserisco le prime 2 dim migliori in Di e le restanti vengono confrontate con altri clusters
    Zi = sortrows(Zi,3);
    D{i} = Zi(1:2,2:3);
    Z = [Z;Zi(3:end,:)];
end

% seleziono le rimanenti dimensioni
Z = sortrows(Z,3);
Zbest = Z(1:l,:);

% inserisco nei rispettivi container Di
for i=1:l
    D{Zbest(i,1)} = [D{Zbest(i,1)}; Zbest(i,2:3)];
end
    
end
