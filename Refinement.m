function [ Cref, Dref, outliers ] = Refinement( dataset, M, C, l, distFun )
%REFINEMENT fase 3: raffinamento dei cluster e delle dimensioni trovate. Gli outliers vengono eliminati
%
% ----- INPUT -----
% M: set medoids ottimali
% C: cluster associati a ciascun medoid
% l: dimensioni rilevanti massime da trovare
% distFun: funzione di distanza usata per confrontare i patterns
%
% ----- OUTPUT -----
% Cref: cluster raffinati
% Dref: distanze raffinate
% outliers: punti che non sono stati associati ai clusters raffinati

% inizializzazione variabili
nDims = size(M,2);
k = size(M,1);

% ricalcolo le dimensioni, stavolta sui clusters anziche' sui vicini 
Dref = findDimensions(M, l*k, C, distFun, dataset);

% calcolo le distanze minime fra i vari centroidi
minDists = ones(nDims,1)*inf;

for i=1:size(M,1)
    mi = M(i,:);
    di = zeros(1,nDims);
    di(Dref{i}(:,1)) = 1;
    
    for j=1:size(M,1)
        mj = M(j,:);       
        if(i~=j)
            dist = distFun(mi,mj,di);
            if(dist < minDists(i))
                minDists(i) = dist;
            end
        end

    end
end

% cluster raffinati e outliers
[Cref, outliers] = assignPoints( dataset, M, Dref, distFun, minDists );

end

