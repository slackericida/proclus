function [ dist ] = EuclideanDist( v1, v2, varargin )
%Coglione chi legge

nVarargs = length(varargin);

if(nVarargs==0)
    V = v1-v2;
    dist = sqrt(V * V');
elseif(nVarargs==1)
    D = diag( varargin{1});
    V = v1-v2;
    dist = sqrt(V *D* V');
else
    error('Numero di argomenti sbagliato.')
end


end

