function [ C, varargout ] = assignPoints( dataset, M, D, distFun, varargin )
%ASSIGNPOINTS Assegna i pattern in dataset al piu' vicino dei medoids contenuti in M,
% in base alla funzione di dissimilarita' distFun, configurata con i pesi in D
%
% ----- INPUT -----
% dataset: dati da clusterizzare
% M: set di medoids
% D: dimensioni rilevanti associate a ciascun medoid
% distFun: funzione di dissimilarita' usata per confrontare i patterns
% maxDist(opzionale): vettore che definisce per ogni medoid la massima distanza accettata: 
%                     se superata il pattern viene marcato come outlier (default=inf -> nessun outlier)
%
% ----- OUTPUT -----
% C: cluster (insieme di punti del dataset) associato a ciascun medoid in M
% outliers(opzionale): insieme di punti che non sono stati associati a nessun cluster

% inizializzazione variabili e strutture dati
nDims = size(dataset,2); % numero di dimensioni del problema
N = size(dataset,1); % dimensione del dataset
k = size(M,1); % numero di medoids
C = zeros(k,N); % set dei cluster ritornati
outliers = zeros(1,N);

% parsing degli argomenti di input
if(nargin == 4)
    maxDist = ones(k,1)*Inf;
elseif(nargin == 5)
    maxDist = varargin{1};
else
    error('Numero argomenti di input sbagliato');
end

% assegno ogni punto "pi" al medoid piu' vicino, a meno che la sua distanza non sia maggiore della soglia minima accettata
for i=1:N
    pi = dataset(i,:);
    min_dist_pi = inf;
    ind_pi = 0;
    
    % cerco il medoid pi� vicino, in base alle rispettive dimensioni
    for j=1:k
        mj = M(j,:);
        dj = zeros(nDims,1);
        dj(D{j}(:,1)) = 1;
        dist = distFun(pi,mj,dj);
        if(dist < min_dist_pi)
            min_dist_pi = dist;
            ind_pi = j;           
        end
    end
    
    % inserisco il pattern nel cluster rispettivo, oppure negli outliers
    if(min_dist_pi > maxDist(ind_pi))
        outliers(i) = 1;
    else
        C(ind_pi, i) = 1;
    end
    
end

% ritorno, se richiesto in fase di chiamata della funzione, l'insieme degli outliers
if(nargout == 2)
    varargout{1} = outliers;
end

end

