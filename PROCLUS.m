close all

% generazione dataset
dataset = unique(DataGen(50, 3, 3, 50),'rows');

% plot
% figure
% scatter3(dataset(1:50,1), dataset(1:50,2), dataset(1:50,3),'b')
% hold on
% scatter3(dataset(51:100,1), dataset(51:100,2), dataset(51:100,3),'r')
% hold on
% scatter3(dataset(101:150,1), dataset(101:150,2), dataset(101:150,3),'g')

% parametri dell'algoritmo
k = 3; % # clusters
A = round(size(dataset,1)*.5); % dimensione sottocampionamento
B = 4; % dimensione pool candidati
l=0; % numero aggiuntivo di dimensioni da considerare = l*k
minDev = 0.1; % valore per filtrare i clusters troppo piccoli (0.1 su paper)
maxIT = 30; % numero massimo di tentativi fatti per trovare il set migliore di medoids
distFun = @ManhattanDist;

% 1 - fase di inizializzazione
M = Initialization( dataset, A, B*k, distFun );
hold on
scatter3(M(:,1), M(:,2), M(:,3),'r', 'filled')
hold off

% 2 - fase iterativa
[ res ] = Iterative( dataset, M, k, l, distFun, minDev, maxIT);

% 3 - fase di raffinamento
Mopt = res{1};
Copt = res{2};
[ Cref, Dref, outliers ] = Refinement( dataset, Mopt, Copt, l, distFun );

figure
scatter3(dataset(:,1), dataset(:,2), dataset(:,3),'b')
hold on
scatter3(Mopt(:,1), Mopt(:,2), Mopt(:,3),'r', 'filled')
hold off

% plot results
figure
ind1 = find(Cref(1,:)==1);
ind2 = find(Cref(2,:)==1);
ind3 = find(Cref(3,:)==1);
scatter3(dataset(ind1,1), dataset(ind1,2), dataset(ind1,3),'b')
hold on
scatter3(dataset(ind2,1), dataset(ind2,2), dataset(ind2,3),'r')
hold on
scatter3(dataset(ind3,1), dataset(ind3,2), dataset(ind3,3),'g')