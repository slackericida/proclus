function [ res ] = Iterative( dataset, M, k, l, distFun, minDev, maxIT )
%ITERATIVE step 2 in cui si effettua l'hill-climbing per selezionare i migliori medoids
%
% ----- INPUT -----
% dataset: dati da clusterizzare
% M: insieme medoids candidati
% k: numero clusters desiderato
% l: numero di dimensioni massimo da selezionare = l*k
% distFun: funzione di dissimilarita' usata per confrontare i patterns
% minDev: soglia usata per scartare medoids associati a clusters poco popolati
% maxIT: numero massimo di tentativi per trovare insieme di medoids ottimali
%
% ----- OUTPUT -----
% res: medoids ottimali trovati e cluster associati

% inizializzazione variabili
currIT = 1; % iterazione corrente
bestObjective = Inf; % funzione obiettivo (da minimizzare)
Mbest = []; % set dei migliori medoids trovati fin'ora
Cbest = []; % cluster associati ai medoids migliori
N = size(dataset,1);

% seleziono k rappresentanti a caso
indM = randperm(size(M,1));
indCurr = indM(1:k); % indice del set corrente dei medoids
indOut = indM(k+1:end); % indice dei restanti medoids, non considerati al momento
Mcurr = M(indCurr,:);

while(1)   
    
    % calcolo vicinato per ciascun medoid
    L=zeros(k,N); % container del vicinato di ciascun medoid 
    for i=1:k;
        mi = Mcurr(i,:);
        di = inf;
        
        % cerco distanza "di" relativa a "mi" dal rappresentante "mj" a lui piu' vicino
        for j=1:size(Mcurr,1)
            if(j~=i)
                dij = distFun(mi, Mcurr(j,:));
                if(dij < di)
                    di = dij;
                end
            end
        end
        
        % cerco tutti i punti del dataset che abbiano dist < "di" da "mi"
        for j=1:N
            dij = distFun(mi, dataset(j,:));
            if(dij < di)
                L(i,j) = 1;
            end
        end        
   
    end
    
    %DEBUG
%     ind1 = find(L(1,:)==1); ind2 = find(L(2,:)==1); ind3 = find(L(3,:)==1);
%     figure
%     scatter3(dataset(ind1,1), dataset(ind1,2), dataset(ind1,3),'b')
%     hold on
%     scatter3(Mcurr(1,1),Mcurr(1,2),Mcurr(1,3),'g','filled')
%     scatter3(Mcurr(2,1),Mcurr(2,2),Mcurr(2,3),'c','filled')
%     scatter3(Mcurr(3,1),Mcurr(3,2),Mcurr(3,3),'y','filled')
%     hold off
%     figure
%     scatter3(dataset(ind2,1), dataset(ind2,2), dataset(ind2,3),'r')
%     hold on
%     scatter3(Mcurr(1,1),Mcurr(1,2),Mcurr(1,3),'g','filled')
%     scatter3(Mcurr(2,1),Mcurr(2,2),Mcurr(2,3),'c','filled')
%     scatter3(Mcurr(3,1),Mcurr(3,2),Mcurr(3,3),'y','filled')
%     hold off
%     figure
%     scatter3(dataset(ind3,1), dataset(ind3,2), dataset(ind3,3),'k')
%     hold on
%     scatter3(Mcurr(1,1),Mcurr(1,2),Mcurr(1,3),'g','filled')
%     scatter3(Mcurr(2,1),Mcurr(2,2),Mcurr(2,3),'c','filled')
%     scatter3(Mcurr(3,1),Mcurr(3,2),Mcurr(3,3),'y','filled')
%     hold off
    
    % trova le dimensioni piu rilevanti per ciascun insieme in L
    D = findDimensions(Mcurr, l*k, L, distFun, dataset);
    
    % assegno i pattern ai cluster in base alle relative dimensioni
    C = assignPoints( dataset, Mcurr, D, distFun );
    
    %DEBUG
%     figure
%     ind1 = find(C(1,:)==1); ind2 = find(C(2,:)==1); ind3 = find(L(3,:)==1);
%     scatter3(dataset(ind1,1), dataset(ind1,2), dataset(ind1,3),'b')
%     hold on
%     scatter3(dataset(ind2,1), dataset(ind2,2), dataset(ind2,3),'r')
%     scatter3(dataset(ind3,1), dataset(ind3,2), dataset(ind3,3),'k')
    
    % valuto la qualita' dei clusters ottenuti
    [ q, R ] = evalClusts( C, D, distFun, minDev, dataset, Mcurr);
    %[ q, R ] = evalClustsMINSOD( C, D, distFun, minDev, dataset);
    hold off
    
    % vedo se sono riuscito a migliorare la qualita' e in caso rimpiazzo la migliore
    % soluzione fin'ora trovata
    if(q < bestObjective)
        bestObjective = q;
        Mbest = Mcurr;
        Cbest = C;
        currIT = 1;
    else
        currIT = currIT+1;
    end
    
    % se non sono riuscito per maxIT a migliorare la qualita', esco dal ciclo
    if(currIT >= maxIT)
        break;
    end
    
    % rimpiazzo i medoid con alcuni nuovi scelti a caso e rimetto gli indici dei cluster scartati   
    % fra quelli non scelti, cosi' da poterli riselezionare dopo
    indAux = indCurr;
    indCurr(R==1) = indOut(1:sum(R));
    indOut(1:sum(R)) = indAux(R==1);
    indOut = indOut(randperm(length(indOut)));
    Mcurr = M(indCurr,:);
    
%     close(2)
%     close(3)
%     close(4)
end

% ritorno la migliore soluzione trovata
res = {};
res{1} = Mbest;
res{2} = Cbest;

end